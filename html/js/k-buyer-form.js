/* форма проверки */

function buyerViewModel(url) {



    var self = this;

    this.url = url;

    this.delivStatus = ko.observable();
    this.delivTime = ko.observable();

    this.orderNum = ko.observable().extend({
        required: true
    });

    this.done = ko.observable(false);
    this.form = ko.observable(true);


    this.submit = function () {
        console.log(this);
        if (this.errors().length == 0) {
            $.ajax(self.url, {
                data: {"json": ko.toJSON(self)},
                type: "get",
                //dataType: "jsonp",
                //crossDomain: true,
                success: function (result) {
                    self.done(true);
                    self.form(false);

                    console.log(result);

                    self.delivStatus(result.text);
                    self.delivTime(new Date( result.time));

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });


        } else {
            // alert('Please check your submission.');
            this.errors.showAllMessages();
        }
    }

    this.errors = ko.validation.group(this);

}

// Activates knockout.js
$(function () {
    var block = $('#k-buyer-form')[0];
    if (!block) return;
    ko.applyBindings(new buyerViewModel('http://crm.deliv.me/orderStatus'), block);
   // alert('q');
});

