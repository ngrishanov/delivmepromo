/* форма для магазинов !  */

function clientViewModel(url) {

    var self = this;

    this.url = url;

    this.company = ko.observable().extend({
        required: true
    });
                
    this.email = ko.observable().extend({
        email: true,
        required: true
    });

    this.phone = ko.observable().extend({
        required: true
    });

    this.city = ko.observable().extend({
        required: true
    });

//    this.site = ko.observable().extend({
//        required: true
//    });

//    this.time1 = ko.observable('10:00');
//    this.time2 = ko.observable('18:00');
//
//    this.timeCall = ko.computed(function() {
//        return  'c: ' + this.time1() + ' до: ' + this.time2();
//    }, this);
//
//    this.averageCost = ko.observable();
//
//    this.siteEngine = ko.observable();
//
//    this.workingWay = ko.observable();
//
//    this.bank = ko.observable();
//
//    this.wishes = ko.observable();

    this.done = ko.observable(false);
    this.form = ko.observable(true);


    this.submit = function () {

        console.log(ko.toJS(self));

	$.support.cors = true;
        if (this.errors().length == 0) {
            $.ajax(self.url, {
                data: {"json": ko.toJSON(self)},
                type: "post",
	            //crossDomain: true,
                //dataType: "jsonp",
                success: function (result) {
                    self.done(true);
                    self.form(false);
                    console.log(result);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });


        } else {
            // alert('Please check your submission.');
            this.errors.showAllMessages();
        }
    }

    this.errors = ko.validation.group(this);

}

// Activates knockout.js
$(function () {
    var block = $('#k-client-form')[0];
    if (!block) return;
    ko.applyBindings(new clientViewModel("http://crm.deliv.me/registerShop"), block);

    //http://jsfiddle.net/echo/jsonp/
});

