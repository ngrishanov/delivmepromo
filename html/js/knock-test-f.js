function formViewModel() {

    var self = this;

    this.progress = ko.observable(10);

    this.curFolder = ko.observable(1);

    this.datePicker = ko.observable(false);

    this.fio = ko.observable()
        .extend({ persist: 'fio' })
        .extend({required: true });

    this.email = ko.observable()
        .extend({ persist: 'email' })
        .extend({ email: true, required: true});


    this.phone = ko.observable()
        .extend({ persist: 'phone' })
        .extend({ required: true, digit: true });

    this.birthday = ko.observable()
        .extend({ persist: 'birthday' })
        .extend({
            required: true,
            date: true
        });

    this.sex = ko.observable('')
        .extend({ persist: 'sex'})
        .extend({
            required: { message: 'Укажите ваш пол' }
        });

    this.educationValues = [
        'начальное',
        'основное',
        'среднее',
        'неполное высшее',
        'высшее'
    ];

    this.education = ko.observable()
        .extend({ persist: 'education' })
        .extend({ required: true });

    this.multipleSelectedOptionValues = [
        '05:00 – 06:00'
    ]

    this.experience = ko.observable()
        .extend({ persist: 'experience' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.mobOs = ko.observable()
        .extend({ persist: 'mobOs' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.weight = ko.observable()
        .extend({ persist: 'weight' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.extras1 = ko.observable()
        .extend({ persist: 'extras1' });

    self.extras2 = ko.observable()
        .extend({ persist: 'extras2' });

    self.extras3 = ko.observable()
        .extend({ persist: 'extras3' });

    self.extras4 = ko.observable()
        .extend({ persist: 'extras4' });

    self.hoursInDay = ko.observable()
        .extend({ persist: 'hoursInDay' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.convenientTime = ko.observable()
        .extend({ persist: 'convenientTime' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.busyTime = ko.observable()
        .extend({ persist: 'busyTime' })
        .extend({ required: { message: 'Необходимо выбрать' } });

    self.workingDays = ko.observableArray()
        .extend({ persist: 'workingDays' });

    this.pff = function () {
        console.log(self.datePicker());
        self.datePicker() ? self.datePicker(false) : self.datePicker(true)
    }

    //  обязательные поля
    var StepValidation1 = [
        self.fio,
        self.email,
        self.birthday,
        self.education,
        self.phone,
        self.sex,
    ];

    var StepValidation2 = [
        self.experience,
        self.mobOs,
        self.weight
    ];
    var StepValidation3 = [
        self.hoursInDay,
        self.convenientTime,
        self.busyTime
    ];
    var StepValidation4 = [];

    this.errors1 = ko.validation.group(StepValidation1);
    this.errors2 = ko.validation.group(StepValidation2);
    this.errors3 = ko.validation.group(StepValidation3);

    this.done = ko.observable(false);
    this.form = ko.observable(true);


    this.errors = ko.validation.group(this);


    this.next = function () {

        switch (self.curFolder()) {
            case 1:
                if (self.errors1().length === 0) {
                    self.curFolder(self.curFolder() + 1);
                    self.progress(38);
                }
                else {
                    self.errors1.showAllMessages();
                    console.log(self.errors1.length);
                }
                break
            case 2:
                if (self.errors2().length === 0) {
                    self.curFolder(self.curFolder() + 1);
                    self.progress(63);
                } else {
                    self.errors2.showAllMessages();
                }
                break
            case 3:
                if (self.errors3().length === 0) {
                    self.curFolder(self.curFolder() + 1);
                    self.progress(87);
                } else {
                    self.errors3.showAllMessages();
                }
                break
            case 4:
                $.ajax("ajax/cy.json", {
                    data: ko.toJSON(self),
                    type: "post", contentType: "application/json",
                    success: function (result) {
                        self.done(true);
                        self.form(false);
                        self.progress(100);
                        self.curFolder(self.curFolder() + 1);
                    }
                });
                break
            default:
                self.progress(100);
        }

    };

    this.back = function () {

        self.curFolder(self.curFolder() - 1);

        switch (self.curFolder() - 1) {
            case 1:
                self.progress(38);
                break
            case 2:
                self.progress(63);
                break
            case 3:
                self.progress(87);
                break
            case 4:
                self.progress(100);
                break
            default:
                self.progress(0);
        }
    }


}

// Activates knockout.js
$(function () {
    ko.applyBindings(new formViewModel());
});

