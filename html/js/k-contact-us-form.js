/* форма обратной связи !  */

function contactViewModel(url) {

    var self = this;

    this.url = url;

    this.fio = ko.observable("фио").extend({
        required: true
    });

    this.email = ko.observable('email@mail.ru').extend({
        email: true,
        required: true
    });

    this.phone = ko.observable();

    this.message = ko.observable().extend({
        required: true
    });

    this.done = ko.observable(false);
    this.form = ko.observable(true);


    this.submit = function () {
        console.log(ko.toJS(self));
        if (this.errors().length == 0) {

            $.ajax( this.url , {
                data: {"json": ko.toJSON(self)},
                type: "post",
                //dataType: "jsonp",
                //crossDomain: true,
                success: function (result) {
                    self.done(true);
                    self.form(false);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });


        } else {
            this.errors.showAllMessages();
        }
    }

    this.errors = ko.validation.group(this);

}

// Activates knockout.js
$(function () {
    var block = $('#k-contact-us-form')[0];
    if (!block) return;
    ko.applyBindings(new contactViewModel("http://crm.deliv.me/writeToUs"), block);
});

